from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from .models import Presentation
import json
from common.json import ModelEncoder
from events.models import Conference
import pika


class PresentationListEncoder(ModelEncoder):
    model = Presentation
    properties = [
        "title",
    ]


class ConferenceListEncoder(ModelEncoder):
    model = Conference
    properties = ["name"]


class PresentationDetailEncoder(ModelEncoder):
    model = Presentation
    properties = [
        "presenter_name",
        "company_name",
        "presenter_email",
        "title",
        "synopsis",
        "created",
    ]
    encoders = {
        "conference": ConferenceListEncoder(),
    }

    def get_extra_data(self, o):
        return {"status": o.status.name}


@require_http_methods(["GET", "POST"])
def api_list_presentations(request, conference_id):
    if request.method == "GET":
        presentations = Presentation.objects.filter(conference=conference_id)
        return JsonResponse(
            {"presentations": presentations}, encoder=PresentationListEncoder
        )
    else:
        content = json.loads(request.body)
        try:
            conference = Conference.objects.get(id=conference_id)
            content["conference"] = conference
        except Presentation.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid presentation"},
                status=400,
            )
        presentation = Presentation.create(**content)
        return JsonResponse(
            presentation,
            encoder=PresentationListEncoder,
            safe=False,
        )


@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_presentation(request, id):
    if request.method == "GET":
        presentation = Presentation.objects.get(id=id)
        return JsonResponse(
            presentation, encoder=PresentationDetailEncoder, safe=False
        )
    elif request.method == "DELETE":
        count, _ = Presentation.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        try:
            presentation = Presentation.objects.get(id=id)
        except Presentation.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Presentation"},
                status=400,
            )
        Presentation.objects.filter(id=id).update(**content)
        presentations = Presentation.objects.get(id=id)
        return JsonResponse(
            presentations,
            encoder=PresentationDetailEncoder,
            safe=False,
        )


@require_http_methods(["PUT"])
def api_approve_presentation(request, id):
    presentation = Presentation.objects.get(id=id)
    presentation.approve()

    parameters = pika.ConnectionParameters(host="rabbitmq")
    connection = pika.BlockingConnection(parameters)
    channel = connection.channel()
    channel.queue_declare(queue="presentation_approval")
    channel.basic_publish(
        exchange="",
        routing_key="presentation_approval",
        body=json.dumps(
            {
                "presenter_name": presentation.presenter_name,
                "presenter_email": presentation.presenter_email,
                "title": presentation.title,
            }
        ),
    )
    connection.close()
    return JsonResponse(
        presentation,
        encoder=PresentationDetailEncoder,
        safe=False,
    )


@require_http_methods(["PUT"])
def api_reject_presentation(request, id):
    presentation = Presentation.objects.get(id=id)
    presentation.reject()
    parameters = pika.ConnectionParameters(host="rabbitmq")
    connection = pika.BlockingConnection(parameters)
    channel = connection.channel()
    channel.queue_declare(queue="presentation_rejection")
    channel.basic_publish(
        exchange="",
        routing_key="presentation_rejection",
        body=json.dumps(
            {
                "presenter_name": presentation.presenter_name,
                "presenter_email": presentation.presenter_email,
                "title": presentation.title,
            }
        ),
    )
    connection.close()
    return JsonResponse(
        presentation,
        encoder=PresentationDetailEncoder,
        safe=False,
    )


# # Set up RabbitMQ connection parameters
# parameters = pika.ConnectionParameters(host="rabbitmq")
# connection = pika.BlockingConnection(parameters)
# channel = connection.channel()

# # Declare the task queues
# channel.queue_declare(queue="presentation_approvals")
# channel.queue_declare(queue="presentation_rejections")


# def send_approval_message(presenter_name, presenter_email, title):
#     # Convert data to JSON and publish to presentation_approvals queue
#     data = {
#         "presenter_name": presenter_name,
#         "email_address": presenter_email,
#         "title": title,
#     }
#     message = json.dumps(data)
#     channel.basic_publish(
#         exchange="", routing_key="presentation_approvals", body=message
#     )


# def send_rejection_message(presenter_name, presenter_email, title):
#     # Convert data to JSON and publish to presentation_rejections queue
#     data = {
#         "presenter_name": presenter_name,
#         "email_address": presenter_email,
#         "title": title,
#     }
#     message = json.dumps(data)
#     channel.basic_publish(
#         exchange="", routing_key="presentation_rejections", body=message
#     )


# @require_http_methods(["PUT"])
# def api_approve_presentation(request, id):
#     presentation = Presentation.objects.get(id=id)
#     presenter_name = presentation.presenter_name
#     presenter_email = presentation.presenter_email
#     title = presentation.title
#     presentation.approve()
#     # Publish approval message to RabbitMQ
#     send_approval_message(presenter_name, presenter_email, title)
#     return JsonResponse(
#         presentation,
#         encoder=PresentationDetailEncoder,
#         safe=False,
#     )


# @require_http_methods(["PUT"])
# def api_reject_presentation(request, id):
#     presentation = Presentation.objects.get(id=id)
#     presenter_name = presentation.presenter_name
#     presenter_email = presentation.presenter_email
#     title = presentation.title
#     presentation.reject()
#     # Publish rejection message to RabbitMQ
#     send_rejection_message(presenter_name, presenter_email, title)
#     return JsonResponse(
#         presentation,
#         encoder=PresentationDetailEncoder,
#         safe=False,
#     )


# # Close the RabbitMQ connection when done
# connection.close()
